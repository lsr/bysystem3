const path = require('path')
const AutoImport = require("unplugin-auto-import/webpack");
const Components = require("unplugin-vue-components/webpack");
const { ElementPlusResolver } = require("unplugin-vue-components/resolvers");
const { defineConfig } = require('@vue/cli-service')
const version = new Date().getTime();

function resolve(dir) {
  return path.join(__dirname, dir)
}

module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave: false,// 关闭语法检查
  configureWebpack: {
    resolve: {
      alias: {
        '@': resolve('src')
      }
    },
    output: {
      filename: `static/js/[name].${version}.js`,
      chunkFilename: `static/js/[name].${version}.js`,
    },
    plugins: [
      AutoImport({
        resolvers: [ElementPlusResolver({
          exclude: new RegExp(/^(?!.*loading-directive).*$/)
        })],
      }),
      Components({
        resolvers: [ElementPlusResolver()],
      }),
    ],
  },
})
