import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { createPinia } from 'pinia'
import CTable from '@/components/CTable'
import CTableColumn from '@/components/CTableColumn'
import CPagination from '@/components/CPagination'
import 'element-plus/dist/index.css'
import "@/styles/index.scss";

const app = createApp(App)
app.use(router)
app.use(createPinia())
app.component(CTable)
app.component(CTableColumn)
app.component(CPagination)
app.mount('#app')
