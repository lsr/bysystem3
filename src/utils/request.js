import axios from 'axios'

const http = axios.create({
  withCredentials: true,
  timeout: 30000
})

//http.interceptors.request.use(config => {
//
//})
//
http.interceptors.response.use(res => {
  return res.data
})

export default http
