import {isRef, isReactive} from 'vue'
export function isEmpty(obj){
  return obj === null || obj === undefined || obj === ''
}

export function setReactive(obj, value){
  if(isRef(obj)) obj.value = value
  else if(isReactive(obj)) obj = value
}

export function getReactive(obj) {
  return isRef(obj) ? obj.value : obj
}

export function filterEmpty(obj) {
  if(obj){
    for(const key of Reflect.ownKeys(obj)){
      if(obj[key] === null || obj[key] === undefined || obj[key] === ''){
        Reflect.deleteProperty(obj, key)
      }
    }
  }
  return obj
}
