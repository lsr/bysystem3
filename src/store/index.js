import {defineStore} from 'pinia'

export const useCommonStore = defineStore('common', {
  state(){
    return {
      username: 'lsr'
    }
  },
  actions: {
    changeUsername(){
      this.username = 'jonas'
    }
  }
})
