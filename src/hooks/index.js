import axios from '@/utils/request'
import {isEmpty, setReactive, filterEmpty, getReactive} from '@/utils'
import qs from 'qs'
import {ElMessage, ElMessageBox} from 'element-plus'
import {ref} from 'vue'

export const useSuccess = () => {
  ElMessage({
    message: "操作成功",
    type: "success",
    duration: 1500,
  })
}

export const useError = (message = '未知异常') => {
  ElMessage({
    message,
    type: 'error',
    duration: 1500
  })
}

export const useTip = (msg, tip = '提示') => {
  return ElMessageBox.confirm(msg, tip, {
    type: 'warning'
  })
}

/**
 * 普通请求
 * @param loading 是否显示loading，有传则显示。注意：loading必须是ref或reactive定义的响应式对象
 * @param url 请求url，不需要传基础路径
 * @param method 请求方法
 * @param params query参数
 * @param data body参数
 * @param after 请求成功的回调函数
 * @param error 请求失败的回调函数
 * @param f
 * @param paramsSerializer 是否序列化query参数，默认为false
 */
export const useAPI = ({loading, url, method, params, data, after, error, f, paramsSerializer = false}) => {
  if(!isEmpty(loading)) setReactive(loading, true)
  axios({
    url,
    method,
    params,
    data,
    paramsSerializer: paramsSerializer ? (params) => {
      return qs.stringify(params, { indices: false })
    } : undefined,
  }).then(d => {
    after && after(d)
  }).catch(e => {
    error && error(e)
  }).finally(() => {
    if(!isEmpty(loading)) setReactive(loading, false)
    f && f()
  })
}

/**
 * 列表查询抽象方法
 * @param loading 是否显示loading，必须是响应式对象
 * @param url 请求url
 * @param page 页码
 * @param limit 分页显示的数量
 * @param method 请求方法
 * @param params query参数
 * @param data body参数
 * @param after 请求成功的回调函数
 * @param error 请求出错的回调函数
 * @param f finally函数
 * @param dataList 列表数据，必须是响应式数据
 * @param total 列表总记录数，必须是响应式数据
 * @param pagination 是否分页，默认为true
 * @param paramsSerializer 是否序列化query参数，默认为false
 */
export const useQueryList = ({loading, url, page, limit = 10, method = 'get', params, data, after, error, f, dataList, total, pagination = true, paramsSerializer = false}) => {
  if(!isEmpty(loading)) setReactive(loading, true)
  axios({
    url,
    method,
    params: {
      page: pagination ? page : undefined,
      limit: pagination ? limit : undefined,
      ...filterEmpty(params)
    },
    paramsSerializer: paramsSerializer ? (params) => {
      return qs.stringify(params, { indices: false })
    } : undefined,
    data
  }).then(d => {
    setReactive(dataList, d?.data ?? [])
    if(pagination){
      setReactive(total, d?.total ?? 0)
      if(page > 1 && d?.data?.length === 0){
        setReactive(page, --page)
        useQueryList({loading, url, page, method, params, data, dataList, total, limit, pagination})
      }
    }
    after && after(d)
  }).catch(e => {
    error && error(e)
  }).finally(() => {
    if(!isEmpty(loading)) setReactive(loading, false)
    f && f()
  })
}

/**
 * 用于声明列表的常用响应式变量，在组件中可以放心解构使用
 * page 当前页码
 * total 总记录数量
 * loading 加载状态
 * dataList 列表数据
 * dataListSelections 列表选中的数据
 */
export const useInitList = () => {
  const page = ref(1)
  const total = ref(0)
  const loading = ref(false)
  const dataList = ref([])
  const dataListSelections = ref([])
  return {page, total, loading, dataList, dataListSelections}
}

/**
 * 点击表格行选中数据（单选）
 * @param row 当前点击的行对象
 * @param dataList 表格的ref
 * @param dataListSelections 表格选中的数组
 */
export const useSingle = (row, dataList, dataListSelections) => {
  const _dataList = getReactive(dataList)
  _dataList.clearSelection();
  if (dataListSelections.length === 1) {
    dataListSelections.forEach(item => {
      if (item === row) {
        _dataList.toggleRowSelection(row, false);
      } else {
        _dataList.toggleRowSelection(row, true);
      }
    });
  } else {
    _dataList.toggleRowSelection(row, true);
  }
}

export const useSelectSingle = (selection, row, dataListRef) => {
  const _dataList = getReactive(dataListRef)
  _dataList.clearSelection()
  if(selection.length === 0) return null
  _dataList.toggleRowSelection(row, true)
}

export const useClearSelection = (dataListRef) => {
  getReactive(dataListRef).clearSelection()
}

/**
 * 点击表格行选中数据(多选)
 * @param row 当前点击的行
 * @param dataList 表格的ref
 */
export const useMultipart = (row, dataList) => {
  getReactive(dataList).toggleRowSelection(row)
}

/**
 * 修改页码
 * @param page 页码
 * @param queryMethod 查询方法的引用
 */
export const usePageChange = (page, queryMethod) => {
  queryMethod(page)
}

/**
 * 表单提交抽象方法
 * @param validate 是否对表单进行校验
 * @param tip 是否进行提示，如果提供这个字段，则进行操作前的提示
 * @param dataFormRef 表单的ref
 * @param loading 是否显示loading
 * @param url 请求url
 * @param method 请求方法
 * @param params query参数
 * @param data body参数
 * @param after 请求成功后的回调函数
 * @param error 请求错误的回调函数
 * @param f finally函数
 */
export const useSubmit = ({validate = true, tip, dataFormRef, loading, url, method = 'post', params, data, after, error, f}) => {
  if(validate){
    getReactive(dataFormRef).validate(valid => {
      if(valid){
        if(tip){
          useTip(tip).then(() => _submit(loading, url, method, params, data, after, error, f)).catch(() => {})
        }else{
          _submit(loading, url, method, params, data, after, error, f)
        }
      }
    })
  }else{
    if(tip){
      useTip(tip).then(() => _submit(loading, url, method = 'post', params, data, after, error, f)).catch(() => {})
    }else{
      _submit(loading, url, method, params, data, after, error, f)
    }
  }
}


//私有方法
function _submit(loading, url, method, params, data, after, error, f) {
  if(!isEmpty(loading)) setReactive(loading, true)
  axios({
    url,
    method,
    params,
    data,
  }).then(d => {
    after && after(d)
  }).catch(e => {
    error && error(e)
  }).finally(() => {
    if(!isEmpty(loading)) setReactive(loading, false)
    f && f()
  })
}
